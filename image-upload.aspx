<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="upload-images.aspx.cs" Inherits="hunghandymanservices.upload_images" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
   
<head runat="server">
   
    <title></title>
   
     <link href="styles/StyleSheet.css" rel="stylesheet" />
     <link href="styles/StyleSheethome.css" rel="stylesheet" />
    <%--must have for preview file upload--%>
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <style>
      
       .innercontent {
    margin: auto;
    padding: 0;
    
}
 .button1 {
            position: relative;
            background-color: #b7b7b7;
            border: none;
            font-size: 12px;
            color: #FFFFFF;
            padding: 4px 25px;
            width: 120px;
            text-align: center;
            -webkit-transition-duration: 0.4s; /* Safari */
            transition-duration: 0.4s;
            text-decoration: none;
            overflow: hidden;
            cursor: pointer;
        }

            /*.button1:hover {
        background-color: #c7c7c7;
       
    }*/
            .button1:after {
                content: "";
                background: #bfbfbf;
                display: block;
                position: absolute;
                padding-top: 300%;
                padding-left: 350%;
                margin-left: -20px !important;
                margin-top: -120%;
                opacity: 0;
                transition: all 0.8s;
            }

            .button1:active:after {
                padding: 0;
                margin: 0;
                opacity: 1;
                transition: 0s;
            }

        .auto-style1 {
            text-align: center;
            height: 235px;
        }
        
        .auto-style3 {
            position: relative;
            font-size: 12px;
            color: #FFFFFF;
            width: 161px;
            text-align: center;
            -webkit-transition-duration: 0.4s;
            transition-duration: 0.4s;
            text-decoration: none;
            overflow: hidden;
            cursor: pointer;
            left: 0px;
            top: 0px;
            height: 38px;
            border-style: none;
            border-color: inherit;
            border-width: medium;
            padding: 4px 25px;
            background-color: #b7b7b7;
        }
        .txtBoxLength {
            width: 150px;
        }
        
        .auto-style4 {
            width: 346px;
        }
        .auto-style5 {
            height: 34px;
        }
        .auto-style6 {
            width: 346px;
            height: 34px;
        }
        
    </style>
    <script type="text/javascript">
        function ImagePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#<%=Image1.ClientID%>').prop('src', e.target.result)
                        .width(250)
                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }                
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="innercontent col-8 col-s-8" style="text-align:left">
            <h1>Steps take before upload images</h1>
            <br />
            <ol>
                <li>Compress images at this link:  <a href="https://www.tinypng.com" target="_blank">www.tinypng.com</a></li>
                <a href="https://www.tinypng.com"></a>
                <li>Upload only file type:  <b>.png, .jpg, .jpeg, .gif</b></li>
            </ol>
           <br />
            <hr />
            <br />
            <br />
            <br />
           <table style="margin-left:15%; margin-right:15%;" border="1">
                <tr>
                    <td class="auto-style5">
                        Select Image to Upload:
                    </td>
                    <td class="auto-style6">
                        <asp:FileUpload ID="FileUpload1" runat="server"  onchange="ImagePreview(this);" class="txtBoxLength" Height="25px" Width="524px"></asp:FileUpload>
                    </td>
                </tr>
               <tr>
                 <%--  <td class="auto-style2">
                       
                   </td>--%>
                   <td class="auto-style1" colspan="2">
                       <asp:Image ID="Image1" runat="server" Width="445px" />
                   </td>
               </tr>
                <tr>
                    <td>
                        Image Category:
                    </td>
                    <td class="auto-style4">
                        <asp:DropDownList ID="DropDownList1" runat="server" Width="285px" Height="25px">
                            <asp:ListItem Text="--Select Category--" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Kitchen" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Bathroom" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Indoor" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Outdoor" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Image Name:
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtImgName" runat="server" class="txtBoxLength" Height="19px" Width="275px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Image Description</td>
                    <td class="auto-style4">

                        <asp:TextBox ID="txtImgDesc" runat="server" TextMode="MultiLine" Rows="5" Width="271px" Height="42px"></asp:TextBox>
                    </td>
                </tr>                        

               <tr>
                   <td style="text-align:center;padding:10px" colspan="2" >
                 <asp:Button ID="btnImgSave" runat="server" Text="Upload and Save" OnClick="btnImgSave_Click" UseSubmitBehavior="false"
                   OnClientClick="this.disabled='true';this.value='Please wait ...';" CssClass="auto-style3"/><br /><br />
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
                       </td>
                   </tr>
            </table>
           
        </div>
    </form>
</body>
</html>
